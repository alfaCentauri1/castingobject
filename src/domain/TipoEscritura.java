package domain;

public enum TipoEscritura {
    CLASICO("Escritura a Mano"),
    MODERNO("Escritura Digital"),
    CUNEIFORME("Escritura Cuneiforme");

    private final String description;

    private TipoEscritura(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
