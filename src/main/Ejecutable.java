package main;

import domain.Empleado;
import domain.Escritor;
import domain.Gerente;
import domain.TipoEscritura;

public class Ejecutable {
    public static void main(String args[]){
        System.out.println("Casting de objetos. ");
        Empleado empleado = new Escritor("Juan", 5000, TipoEscritura.CLASICO);
        Empleado empleado2 = new Gerente("Pedro", 7500, "Administración");
        //
        System.out.println("Sin el Casting de objetos: ");
        System.out.println(empleado.obtenerDetalles());
        //DownCasting
        Escritor escritor = (Escritor) empleado;
        System.out.println(escritor.obtenerDetalles());
        //
        Gerente gerente = (Gerente) empleado2;
        System.out.println(gerente.obtenerDetalles());
        //Upcasting
        Empleado empleado3 = escritor;
        empleado3.setNombre("Maria");
        empleado3.setSueldo(5001);
        System.out.println(empleado3.obtenerDetalles());
    }
}
